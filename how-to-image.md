---
layout: page
title: Instructions to flash an image to an SD card
date: 2021-05-01 16:06:23 +0200
permalink: /how-to-image/
---

1. The first thing you have to do is to fetch the right image for your
   Raspberry Pi. Please *make sure you know which model you have*; if
   you download an image built for a higher family, your board will
   fail to boot; if you download one built for a lower one, your
   computer's performance will be reduced.

	For this example, we will assume you are interested in a Raspberry
    Pi model 3.

2. Download the relevant image, either from the [daily built
   images](/daily-images) or from the [tested
   images](/tested-images) webpages. The downloaded image will have a
   name matching <code
   class="highlighter-rouge"><em>builddate</em>_raspi_<em>model</em>_<em>release</em>.img.xz</code>, where
   <code class="highlighter-rouge"><em>builddate</em></code> is the date in CCYYMMDD format the image was 
   generated, <code class="highlighter-rouge"><em>model</em></code> can be `1` (for both `0w` and `1`),
   `2`, `3` or `4` and <code class="highlighter-rouge"><em>release</em></code> is (currently) either `buster` or 
   `bullseye` — In our case resulting in (f.e.), `20210408_raspi_3_bullseye.img.xz`.

3. Download the corresponding *sha256* checksum file. This is a much
   smaller file, only a line of text. So in our case you want `20210408_raspi_3_bullseye.xz.sha256`.

4. Verify both files match:

		$ sha256sum --check 20210408_raspi_3_bullseye.xz.sha256
        20210408_raspi_3_bullseye.xz: OK

   If you get a different result, something is off. Verify your
   download, or notify us!

5. Note the corresponding device for your SD card in your
   computer. 

   **This is a critical step!**  
   If you give the wrong device name, *you can wipe your computer's install*.  
   Please read [how to find your SD card's device](/find-your-sd) if you are not sure.

6. Now, write the image to your SD card! Make sure you got the right
   device for the SD card! (you risk losing information otherwise!)  
   This has to be done as a privileged user:

        $ xzcat 20210408_raspi_3_bullseye.xz | sudo dd of=/dev/{YOUR_DEVICE} bs=64k oflag=dsync status=progress

   Remember `xzcat` is decompressing its input; the reported output size
   will be 1.5GB long, even if your downloaded file is only some
   hundreds of MB.

7. That should be it! Wait for the above command to finish (should
   take no more than a couple of minutes), take your SD card out, put
   it into your Raspberry, boot and enjoy!

Of course, you will probably be interested on [what are your installed
defaults and configuration settings](/defaults-and-settings).

